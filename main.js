document.addEventListener("DOMContentLoaded", () => {
  const noteForm = document.getElementById("note-form");
  const noteTitle = document.getElementById("note-title");
  const noteContent = document.getElementById("note-content");
  const notesContainer = document.getElementById("notes");

  noteForm.addEventListener("submit", (e) => {
    e.preventDefault();
    addNote();
  });

  function addNote() {
    const note = {
      id: Date.now(),
      title: noteTitle.value,
      content: noteContent.value,
    };
    noteTitle.value = "";
    noteContent.value = "";
    saveNote(note);
    displayNote(note);
  }

  function saveNote(note) {
    let notes = localStorage.getItem("notes");
    notes = notes ? JSON.parse(notes) : [];
    notes.push(note);
    localStorage.setItem("notes", JSON.stringify(notes));
  }

  function displayNote(note) {
    const noteElement = document.createElement("div");
    noteElement.classList.add("p-4", "bg-white", "rounded", "shadow");
    noteElement.innerHTML = `
      <h2 class="text-xl mb-2">${note.title}</h2>
      <p class="mb-2">${note.content}</p>
      <button class="text-red-500" onclick="deleteNote(${note.id})">Delete</button>
    `;
    notesContainer.appendChild(noteElement);
  }

  window.deleteNote = function (id) {
    let notes = localStorage.getItem("notes");
    notes = notes ? JSON.parse(notes) : [];
    notes = notes.filter((note) => note.id !== id);
    localStorage.setItem("notes", JSON.stringify(notes));
    displayNotes();
  };

  function displayNotes() {
    notesContainer.innerHTML = "";
    let notes = localStorage.getItem("notes");
    notes = notes ? JSON.parse(notes) : [];
    notes.forEach((note) => displayNote(note));
  }

  displayNotes();
});
